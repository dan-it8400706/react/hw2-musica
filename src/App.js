import style from './index.scss'
import Header from "./components/header/Header";
import { useState, useEffect }  from "react";
import ListProducts from './components/listProducts/ListProduct';


function App() {
  

  const [cartCounter,setCartCounter]=useState(JSON.parse(localStorage.getItem('cart')) || [])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cartCounter))},[cartCounter]);


  const Add = ()=>{
    setCartCounter (cartCounter => {
      console.log('click Add')
      return [...cartCounter, 1]
        })
  }
  

  return (

    
      <div className={style.main}>
    
        <Header cartCounter={cartCounter.length}/>
    
        <ListProducts Add={Add}/>
        
      </div>

    )
}

export default App