
import style from "./Modal.module.scss";
import Button from "../buttons/Button";

function Modal(props) {

    return (


        <div className={style.wrapper__modal} 
            onClick ={props.actions}>
            <div className={style.modal}>
                <h3 className={style.h3}>{props.header}
                    <span onClick={props.actions}>X</span>
                </h3>
                <p className={style.p}>{props.text}</p>
                <Button 
                    background={props.background}
                    backgroundButton={props.backgroundButton}
                    onClick={props.Add} 
                    text={props.buttonFirstText}
                ></Button>
            </div>
        </div>
    )
}
export  default Modal
