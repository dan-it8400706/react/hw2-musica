import Basket from "../Basket/Basket"
import style from './Header.module.scss'

function Header ({cartCounter}){
    return(
        <header className={style.header}> 
            <Basket cartCounter={cartCounter}/>
        </header>
    )
}

export default Header