import PropTypes from 'prop-types'
import { useState } from 'react'
import style from './Product.module.scss'
import Button from '../buttons/Button'
import { ReactComponent as Star } from './star.svg';
import Modal from '../modals/Modal';





function Product (props){


// Перемикач стану Modal

    const [openModal, setOpenModal] = useState(false)
    
    function ToggleModal(){
        openModal!==true ? setOpenModal(true):setOpenModal(false);
        return openModal
    }

// Перемикач Star, обране (зміна кольору зірки onClick)
    const [fill, setColor] = useState ('#000')

    const ChangeStar = ()=>{
        if(fill==="#000"){
            setColor("#ff0")
        }else{
            setColor("#000")
        }
        console.log (setColor)
    }

    return(
        <li key={props.index} id={props.index}>
            <p className={style.title}>
                {props.name}
                <Star className={style.star} fill={fill} width='25'height='25' onClick={ChangeStar}/>
            </p>
            <img src={props.image} alt="image_of_product" ></img>
            <p>{props.price} $</p>
            <Button 
                onClick ={ToggleModal} 
                backgroundButton = "green" 
                text = "Add to basket"
            />
            { openModal && <Modal 
                backgroundButton='green'
                header="Додавання товару до кошика"
                text = "Ви бажаєте додати товар до кошику?"
                buttonFirstText="OK"
                actions={ToggleModal}
                Add = {props.Add}
                id = {props.index}
            />}  

        </li>
    )
}


Product.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageURL: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
}
export default Product