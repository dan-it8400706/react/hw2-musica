import Product from "../product/Product";
import { useState, useEffect } from "react";
import style from './ListProduct.module.scss'



function ListProducts (props){
    
    const [products, setProducts] = useState([])

    useEffect(()=>{
      const url='https://dummyjson.com/products?limit=10'
      fetch(url)
        .then(res => res.json())
  
        .then(data => {
          console.log(data)
          setProducts(data.products)
        });
      
    },[])


    return(
        <ul className={style.ul}>
            {products.map(p => <Product key={p.title+p.index} name={p.title} index={p.id} price={p.price} image={p.images[0]} Add={props.Add}/>)}
        </ul>
    
    )
}

export default ListProducts